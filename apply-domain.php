<?php
/**
 * @file apply-domain
 * @see 
 */
define('TEST', false);
define('DEBUG', false);
$nginx_conf_dir = "/etc/nginx/conf.d/";
$cert_dir = "/etc/letsencrypt/live/";
include "./../../../wp-load.php";
$table = "sonub_domain_application";

/**
 * Check nginx exist in the path. @see https://docs.google.com/document/d/1nOEJVDilLbF0sNCkkRGcDwdT3rDLZp3h59oQ77BIdp4/edit#heading=h.hwkgmnsdn9qc
 */
$output = `nginx -t 2>&1`;
if ( strpos( $output, "syntax is ok" ) === false ) {
	echo "\n------> Output:\n $output\n\n";
	if ( strpos( $output, "no nginx" ) !== false || strpos( $output, "command not found") !== false ) {
		echo "Error - Nginx is not in path. Consider to soft link into (/usr/bin:/bin) or read Cron error message. Look for X-Cron-Env: (PATH)\n";
		domain_log("-", "nginx is not in path");
		exit(-11);
	} else {
		echo "Error: apply-domain.php has error. Nginx configuration is not valid";
		domain_log("-", "nginx configuration is not valid");
		exit(-10);
	}
}

/**
 * Get a domain
 */
global $wpdb;
$row = $wpdb->get_row("SELECT * FROM $table WHERE status=''", ARRAY_A);
if ( ! $row ) return;
if ( ! $row['domain'] ) return;


/**
 * Prepare vars
 */
$domain = $row['domain'];
set_status( $row['idx'], 'P', 'Start application');
if ( DEBUG ) print_r($row);
$conf = "$domain.conf";
$nginx_conf = $nginx_conf_dir . $conf;
$idx = $row['idx'];

                                        /** Test => Check if certs exists for the domain */
                                        if ( DEBUG ) echo "Check certs exists on $cert_dir$domain\n";

/**
 * If certs exists, fine. Simply don't get it.
 */
if ( file_exists( $cert_dir . $domain ) ) {
	set_status($idx, 'A', 'Certbot certs folder already exists');
} else {
	set_status($idx, 'G', 'Going to obtain certs');
					if ( DEBUG ) echo "Going to obtain certs\n";
	$output = `certbot certonly --nginx -d {$domain} 2>&1`;
					if ( DEBUG ) echo $output . "\n";
	/** Got response from Certbot */
	set_status($idx, 'R', "Got response from cert_bot: \n$output");
	if ( strpos($output, "Congratulations!") !== false ) {
		if ( file_exists( $cert_dir . $domain ) ) {
			set_status( $idx, 'C', 'got certificates' );
		} else {
			set_status( $idx, 'F', 'Got certificates. But no files on /etc/letsencrypt/live folder?' );
			exit(-9);
		}
	} else {
		set_status( $idx, 'F', 'failed to obtain certificates' );
		exit(-3);
	}
}

/**
 * Check if nginx conf exists for the domain. It's fine. Simple don't create the domain.
 */
                    if ( DEBUG ) echo "Check nginx conf exists on $nginx_conf\n";
if ( file_exists( $nginx_conf ) ) {
	set_status( $idx, 'S', "nginx configuration exists for the domain - $domain. It's fine.");
	exit(0);
}



$config =<<<EOC
server {
 server_name $domain;
 ssl_certificate /etc/letsencrypt/live/$domain/fullchain.pem;
 ssl_certificate_key /etc/letsencrypt/live/$domain/privkey.pem;
 include user-site.conf;
}
EOC;

$re = file_put_contents($nginx_conf, $config);
if ( DEBUG ) echo "Creating $nginx_conf\n";
if ( $re === false ) {
	if ( DEBUG ) echo "Failed to create nginx configuration file\n";
	set_status( $idx, 'F', 'failed to write nginx configuration');
	exit(-4);
} else {
	set_status( $idx, 'N', 'wrote nginx configuration' );
}

if ( DEBUG ) echo "Testing nginx configuration syntax\n";
$output = `nginx -t 2>&1`;
if ( strpos( $output, 'syntax is ok' ) === -1 ) {
	set_status( $idx, 'F', 'nginx configuration syntax error');
	exit(-5);
}


if ( DEBUG ) echo "Restarting nginx\n";
$output = `nginx -s reload 2>&1`;
$output = trim($output);
if ( $output ) {
	if ( DEBUG ) echo $output . "\n";
	set_status( $idx, 'F', 'failed to restart nginx');
	exit(-6);
}

if ( DEBUG ) echo "Success\n";
set_status($idx, 'S', 'success');
exit(0);



function set_status($idx, $status, $reason) {
	global $wpdb, $table, $domain;
	$wpdb->update( $table, ['status' => $status, 'reason' => $reason, 'stamp_commit' => time()], ['idx' => $idx]);
	domain_log($domain, $reason);
}


function domain_log($domain, $message) {
	global $wpdb;
	$wpdb->insert( 'sonub_domain_application_log', ['domain' => $domain, 'message' => $message, 'stamp' => time()] );
}



