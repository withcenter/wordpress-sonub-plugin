<?php
define('MAX_SITES', 100);
define('MAX_DOMAINS', 200);

$sonub_config = [];

$sonub_config['default_domains'] = [
    'sonub.com',
    'philgo.net'
];


/**
 * Returns sonub system settings.
 * @return array
 */
function sonub_config() {
    global $sonub_config;

    $c = $sonub_config;
    $c[MAX_SITES_CODE] = MAX_SITES;
    $c[MAX_DOMAINS_CODE] = MAX_DOMAINS;
#    $cats = get_categories(['hide_empty' => false]);
	$cats = [];
    if ( $cats ) {
        $categories = [];
        foreach( $cats as $cat ) {
          $n = [];
          $n['id'] = $cat->cat_ID;
          $n['count'] = $cat->count;
          $n['description'] = $cat->category_description;
          $n['link'] = '';
          $n['name'] = $cat->name;
          $n['slug'] = $cat->slug;
          $n['taxonomy'] = $cat->taxonomy;
          $n['parent'] = $cat->parent;
          $n['meta'] = [];
          $n['_link'] = '';
          $categories[] = $n;
        }
        $c['categories'] = $categories;
    }

//    debug_log('sonub_config', $c);

    return $c;
}
