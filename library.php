<?php
if ( defined('SITE_CONFIG') ) return;
include_once 'config.php';

define('COMMENT_ATTACHMENT', 'comment_attachment');

define('SITE_CONFIG', 'sonub_site_config');
define('SITE_CONFIG_META', 'sonub_site_config_meta');
define('DOMAIN_TABLE', 'sonub_domain_application');
define('MAX_SITES_CODE', 'max_sites');
define('MAX_DOMAINS_CODE', 'max_domains');
define('AVAILABLE_SITES_CODE', 'available_sites');
define('AVAILABLE_DOMAINS_CODE', 'available_domains');
define('NO_OF_DOMAINS_IN_PROGRESS', 'no_of_domains_in_progress');

define('SLUG_COUNT', 'slug_count');
define('TERMS_ID_IN_ORDER', 'terms_id_in_order');


define('SONUB_ROUTE_PATH', 'sonub/v2019');
define('ERROR_LOGIN_FIRST', 'login_first');
define('ERROR_BAD_PARAM', 'bad_param');
define('ERROR_MAX_SITES', 'exceed_max_sites');
define('ERROR_MAX_DOMAINS', 'exceed_max_domains');
define('ERROR_DOMAIN_EXISTS', 'domain_is_in_use');
define('ERROR_DOMAIN_INSERT', 'domain_insert');
define('ERROR_MALFORMED_DOMAIN', 'malformed_domain');
define('ERROR_NOT_YOUR_SITE', 'not_your_site');
define('ERROR_NOT_YOUR_DOMAIN', 'not_your_domain');
define('ERROR_DOMAIN_SETUP_IN_PROGRESS', 'domain_setup_in_progress');
define('ERROR_DOMAIN_SETUP_NOT_YET_BEGIN', 'domain_setup_not_yet_begun');
define('ERROR_DEFAULT_DOMAIN', 'default_domain');
define('ERROR_SLUG_COUNT_UPDATE', 'slug_update');
define('ERROR_SLUG_CREATE', 'slug_create');
define('ERROR_SLUG_CREATE_PARENT', 'slug_create_parent');
define('CODE_NOT_YOUR_CATEGORY', 'not_your_category');








function db() {
    global $wpdb;
    return $wpdb;
}

function db_count( $table, $cond = 1 ) {
    $re = db()->get_var("SELECT COUNT(*) FROM " . DOMAIN_TABLE . " WHERE $cond");
    if ( $re ) return intval($re);
    else return 0;
}

function debug_log( $message, $vars = null ) {
    if( WP_DEBUG === true ){
        if( is_array( $message ) || is_object( $message ) ){
            $message = print_r( $message, true );
        }
        if( is_array( $vars ) || is_object( $vars ) ){
            $vars = print_r( $vars, true );
        }
        error_log( $message . ' ' . $vars );
    }
}


/**
 * Return true on windows and mac book.
 * @return bool
 */
function isLocalhost() {
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' || PHP_OS === 'Darwin') { // Windows or Mac
        return true;
    } else {
        return false;
    }
}

/**
 * Return true if 'index.php' is request.
 *
 * @desc not json api, not wp-admin.
 * @return bool
 */
function isRequestingIndex() {
    return strpos( $_SERVER['REQUEST_URI'], '/wp-json/' ) === false
        && strpos( $_SERVER['REQUEST_URI'], 'wp-admin' ) === false;
}

/**
 * Returns sites of the user
 *
 * @param $user_ID
 * @return array
 */
function sites($user_ID) {
    $rows = db()->get_results("SELECT * FROM " . SITE_CONFIG . " WHERE user_ID=$user_ID", ARRAY_A);
    if ( ! $rows ) return [];
    $sites = [];
    foreach( $rows as $site ) {
        $site = site_pre($site);
        if ( $site ) $sites[] = $site;
    }
    return $sites;
}


/**
 * @param int $idx_site
 * @return array
 */
function site($idx_site) {
    $row = db()->get_row("SELECT * FROM " . SITE_CONFIG . " WHERE idx=$idx_site", ARRAY_A);
    return site_pre($row);
}

/**
 * Returns pre-processed site data.
 * @param $site
 * @return array
 *      site data
 *      empty array if no site data.
 */
function site_pre($site) {
    debug_log('site_pre: ', $site);
    if ( $site ) {
        $site = array_merge( siteConfigAll($site['idx']), $site );
        debug_log('site after merge: ', $site);
        /**
         * Domains that are connected to the site.
         */
        $site['domains'] = domains_by_site_idx( $site['idx'] );


        /**
         * Get & Sort categories(menu)
         */
        $cat = get_category_by_slug( "site-$site[idx]" );

        if ( $cat ) {
            $site['root_category_term_id'] = $cat->term_id;
            $categories = siteCategories($site['root_category_term_id']);
            if ( isset($site[TERMS_ID_IN_ORDER]) && $site[TERMS_ID_IN_ORDER] ) {
                $categories = siteSortCategory($categories, $site[TERMS_ID_IN_ORDER]);
            }
            $site['categories'] = $categories;
        }


        return $site;
    } else {
        return [];
    }
}

/**
 * Return true if it is my site.
 * @param $idx_site
 * @param $user_ID
 * @return bool
 */
function siteMine($idx_site, $user_ID) {
    $site = site( $idx_site );
    if ( $site && $site['user_ID'] == $user_ID ) return true;
    else return false;
}

/**
 * Returns true if the domain is belongs to the user_ID
 * @param $domain
 * @param $user_ID
 * @return bool
 */
function domainMine($domain, $user_ID) {
    $row = domainGet($domain);
    if ( $row ) return $row['user_ID'] == $user_ID;
    else false;
}

/**
 * Deletes the domain from domain table.
 * @warning it does not check permission.
 * @param $domain
 */
function domainDelete($domain) {
    db()->delete(DOMAIN_TABLE, ['domain' => $domain]);
}


/**
 * Returns domains by user ID.
 * @use to know all the domains of the user.
 * @param $user_ID
 * @return array|null|object
 */
function domains_by_user_ID( $user_ID ) {
    $rows = db()->get_results("SELECT domain, status, reason FROM " . DOMAIN_TABLE . " WHERE user_ID=$user_ID", ARRAY_A);
    if ( $rows ) {
        return $rows;
    } else {
        return [];
    }
}

/**
 * Returns domains that are connected to a site.
 * @use to know which domains are connected to the site.
 * @param $idx_site
 * @return array|null|object
 */
function domains_by_site_idx( $idx_site ) {
    $rows = db()->get_results("SELECT domain, status, reason FROM " . DOMAIN_TABLE . " WHERE idx_site=$idx_site", ARRAY_A);
    if ( $rows ) {
        return $rows;
    } else {
        return [];
    }
}



/**
 * Gets a config
 *
 * @param int $idx_site - sonub_site.idx which is the site record.
 * @param string $code - code to search.
 * @param string $field - which field to get
 * @return mixed
 *      string as value if found. It can be empty string.
 *      or null if record does not exists.
 *
 * @desc to make it sure if config exists or not, use blogConfigExists()
 */
function siteConfig( $idx_site, $code, $field='value' ) {
    return db()->get_var( "SELECT $field FROM " . SITE_CONFIG_META . " WHERE idx_site=$idx_site AND code='$code' " );
}

/**
 * @param int $idx_site
 * @param string $code
 * @return mixed
 *      true if code exists.
 *      false if not.
 */
function siteConfigExists( $idx_site, $code ) {
    return !! siteConfig($idx_site, $code, 'idx');
}

/**
 * @param int $idx_site
 * @return array
 */
function siteConfigAll( $idx_site ) {
    $rows = db()->get_results( "SELECT code, value FROM " . SITE_CONFIG_META . " WHERE idx_site=$idx_site ", ARRAY_A );
    if ( ! $rows ) return [];
    $confs = [];
    foreach( $rows as $row ) {
        $confs[$row['code']] = $row['value'];
    }
    return $confs;
}

/**
 * Saves code/value on the idx_site.
 *
 * @param int $idx_site
 * @param string $code
 * @param string $value
 */
function siteConfigSave( $idx_site, $code, $value ) {
//    debug_log("if ( siteConfigExists($idx_site, $code) ) {", siteConfigExists($idx_site, $code));
    if ( siteConfigExists($idx_site, $code) ) {
//        debug_log("updating on idx: $idx_site, code: $code, value: $value");
        db()->update(SITE_CONFIG_META, ['value' => $value], ['idx_site' => $idx_site, 'code' => $code]);
    } else {
//        debug_log('false???');
        db()->insert(SITE_CONFIG_META, ['idx_site' => $idx_site, 'code' => $code, 'value' => $value]);
    }
}

/**
 * Saves array of code/value on the idx_site.
 * @param int $idx_site
 * @param array $arr
 */
function siteConfigSaveArray( $idx_site, $arr ) {
    if ( $arr ) {
        foreach( $arr as $k => $v ) {
            siteConfigSave( $idx_site, $k, $v );
        }
    }
}



/**
 * Returns the no of sites created by the user ID.
 * @param $user_ID
 * @return null|string
 */
function siteCount( $user_ID ) {
    return db()->get_var("SELECT COUNT(*) FROM " . SITE_CONFIG . " WHERE user_ID=$user_ID");
}

/**
 * Returns the no of domains created by the user ID.
 * @param $user_ID
 * @param string $status - search for status.
 * @return null|string
 */
function domainCount( $user_ID, $status = '' ) {
    if ( $status ) {
        $status = "AND status='$status'";
    }
    return db()->get_var("SELECT COUNT(*) FROM " . DOMAIN_TABLE . " WHERE user_ID=$user_ID $status");
}
function domainCountProgress( $user_ID ) {
    return db_count(DOMAIN_TABLE, "user_ID=$user_ID AND status!='F' AND status!='S'" );
}

/**
 * Returns true if the domain is already exists. owned.
 * @param string $domain
 * @return null|string
 */
function domainExists( $domain ) {
    return db()->get_var("SELECT COUNT(*) FROM " . DOMAIN_TABLE . " WHERE domain='$domain'");
}





/**
 * This insert a domain into `sonub_domain_application` table, which means it will try to connect CertBot to get certs.
 *
 * @note If the domain is ending with `.sonub.com`, then it does not get certs since wildcard cert is available.
 * @note It does not check errors. So, all error check must be done before invoking this method.
 *
 * @param $domain
 * @param $idx_site
 * @param $user_ID
 * @return false|int
 *
 * @example
 *      if ( ! domainApply( $domain, $idx_site, $user->ID ) ) { ... error ... }
 */
function domainApply( $domain, $idx_site, $user_ID ) {
    $in = ['idx_site' => $idx_site, 'user_ID' => $user_ID, 'domain' => $domain, 'stamp_apply' => time() ];
    if ( isSonubDomain( $domain ) ) {
        $in['status'] = 'S';
    }
    return db()->insert(DOMAIN_TABLE, $in);
}
function domainCreate($domain, $idx_site, $user_ID) {
    return domainApply( $domain, $idx_site, $user_ID );
}

/**
 * Attaches a domain to a site.
 * @attention it does all the necessary checks and if there is an error, it returns WP_Error
 *
 * @param $in
 *      'idx_site' is the site.ix
 *      'domain' is the domain
 *
 * @return WP_Error
 */
function domainAttachToSite($in) {

    $domain = $in['domain'];
    $idx_site = $in['idx_site'];
    if ( ! is_user_logged_in() ) return error(ERROR_LOGIN_FIRST, 'login first');
    if ( ! $idx_site ) return error(ERROR_BAD_PARAM, 'site no is not provided');
    if ( ! $domain ) return error(ERROR_BAD_PARAM, 'domain is not provided');

    if ( ! isValidDomain( $domain ) ) return error(ERROR_MALFORMED_DOMAIN, "You have input wrong domain");
    if ( domainExists( $domain ) ) return error(ERROR_DOMAIN_EXISTS, "Domain name is already in use");

    $user = wp_get_current_user();
    if ( domainCount( $user->ID ) >= MAX_DOMAINS ) return error(ERROR_MAX_DOMAINS, "You cannot add more than " . MAX_DOMAINS . " domains");

    if ( isDefaultDomain($domain) ) return error(ERROR_DEFAULT_DOMAIN, 'You cannot add default domain.');

    if ( ! domainApply( $domain, $idx_site, $user->ID ) ) {
        return error(ERROR_DOMAIN_INSERT, 'Site created! But failed to connect to domain.');
    }
}

/**
 * Returns true if the domain is same as one of default domains or www of the default domains.
 * @param $domain
 * @return bool
 */
function isDefaultDomain($domain) {
    global $sonub_config;
    $defaults = $sonub_config['default_domains'];
    foreach( $defaults as $d ) {
        if ( $domain == $d || $domain == "www.$d" ) return true;
    }
    return false;
}


/**
 * Return true if the domain looks okay.
 * @param $domain
 * @return bool
 */
function isValidDomain($domain) {
    $domain = rtrim($domain, '.');
    if (!mb_stripos($domain, '.')) {
        return false;
    }
    $domain = explode('.', $domain);
    $allowedChars = array('-');
    $extenion = array_pop($domain);
    foreach ($domain as $value) {
        $fc = mb_substr($value, 0, 1);
        $lc = mb_substr($value, -1);
        if (
            hash_equals($value, '')
            || in_array($fc, $allowedChars)
            || in_array($lc, $allowedChars)
        ) {
            return false;
        }
        if (!ctype_alnum(str_replace($allowedChars, '', $value))) {
            return false;
        }
    }
    if (
        !ctype_alnum(str_replace($allowedChars, '', $extenion))
        || hash_equals($extenion, '')
    ) {
        return false;
    }
    return true;
}

/**
 * Returns no of available site for the user.
 * @param $user_ID
 * @return int|null|string
 */
function availableSites( $user_ID ) {
    $count = db()->get_var("SELECT COUNT(*) FROM " . SITE_CONFIG . " WHERE user_ID=$user_ID");
    if ( ! $count ) $count = 0;
    return MAX_SITES - $count;
}

/**
 * Returns no of available domains for the user.
 * @param $user_ID
 * @return int|null|string
 */
function availableDomains( $user_ID ) {
    $count = db()->get_var("SELECT COUNT(*) FROM " . DOMAIN_TABLE . " WHERE user_ID=$user_ID");
    if ( ! $count ) $count = 0;
    return MAX_DOMAINS - $count;
}

/**
 * Return true if the domain is sonub domain.
 * @param $domain
 * @return bool
 */
function isSonubDomain( $domain ) {
    return stripos( $domain, ".sonub.com") > 1;
}

/**
 * Create(Insert) a site data into site config.
 * @param $options
 * @see
 * @return int - site.idx is returned.
 */
function siteCreate( $options ) {
    db()->insert( SITE_CONFIG, ['user_ID' => $options['user_ID'], 'stamp_create' => time(), 'stamp_update' => time()] );
    $idx_site = db()->insert_id;
    siteConfigSaveArray( $idx_site, $options );
    return $idx_site;
}

/**
 * Returns a domain record.
 * @param $domain
 * @return array|null|object - null on failure
 *
 * @use this to know the site.idx of the domain.
 * @see domainSite() to get site config of the domain.
 */
function domainGet($domain) {
    return db()->get_row("SELECT * FROM " . DOMAIN_TABLE . " WHERE domain='$domain'", ARRAY_A);
}
function domainSite($domain) {
    $dom = domainGet($domain);
    if ( $dom ) {
        return site($dom['idx_site']);
    }
    return [];
}


/**
 * Returns all of the categories of a site in short format.
 * @param $idx_site
 * @return array
 */
function siteCategories($root_term_id) {

    if ( !$root_term_id ) return [];
    $args = array(
        'parent'                 => $root_term_id,
        'hide_empty'             => false,
    );
    $child_categories = get_categories($args );

    $res = [];
    foreach( $child_categories as $c ) {
        $res[] = ['term_id' => $c->cat_ID, 'name' => $c->name, 'slug'=>$c->slug];
    }

    return $res;
}

/**
 * Returns all the posts of a site.
 * @param $idx_site
 */
function sitePosts($idx_site) {

}

/**
 * Sorts categories in the order of orders.
 * @warning categories that are not in orders will be added at the end.
 * @param $categories
 * @param $orders
 * @return array
 */
function siteSortCategory($categories, $orders)
{
    $new_categories = [];
    if ($categories) {
        // get categories in order list
        $orders = explode(',', $orders);
        foreach ($orders as $id) {
            foreach ( $categories as $category ) {
                if ( $category['term_id'] == $id ) {
                    $new_categories[] = $category;
                }
            }
        }
        // get categories that are not in order list. add at the end.
        foreach ( $categories as $category ) {
            if ( ! in_array( $category['term_id'], $orders ) ) {
                $new_categories[] = $category;
            }
        }
    }
    return $new_categories;
}


function isRootSite() {
	$sonub_config = sonub_config();
	$domain = $_SERVER['HTTP_HOST'];
	foreach ( $sonub_config['default_domains'] as $rootDomain ) {
		if ( $domain == $rootDomain || $domain == "www.$rootDomain" ) return true;
	}
	if ( isLocalhost() ) {
	    if ( $domain == 'root.sonub.com' ) return true;
    }
	return false;
}


/**
 * Returns site settings which description.
 * @return array
 * @example of return array
        Array
        (
            [0] => Array
            (
                [idx] => 4
                [idx_site] => 1
                [code] => description
                [value] => This is description
                [domains] => Array
                (
                    [0] => my.domain.com
                )
                [stamp_update] => 1540748714
            )
            [1] => Array
            (
                [idx] => 28
                [idx_site] => 9
                [code] => description
                [value] => This is a site of user 1
                [domains] => Array
                (
                    [0] => uuuo.sonub.com
                    [1] => site.sonub.com
                    [2] => site2.sonub.com
                )
                [stamp_update] => 1540748714
            )
        )
 */
function getActiveSites() {
    global $wpdb;
    $sites = [];
    $rows = $wpdb->get_results("SELECT * FROM " . SITE_CONFIG_META . " WHERE code='description' AND `value` != '' ", ARRAY_A );
    if ( ! $rows ) return $sites;
    foreach( $rows as $site ) {
        $site['domains'] = [];
        $site['stamp_update'] = $wpdb->get_var("SELECT stamp_update FROM " . SITE_CONFIG . " WHERE idx=$site[idx_site] ");
        $domains = $wpdb->get_results("SELECT * FROM " . DOMAIN_TABLE . " WHERE idx_site=$site[idx_site] ", ARRAY_A );
        if ( $domains ) {
            foreach( $domains as $domain ) {
                $site['domains'][] = $domain['domain'];
            }
            $sites[] = $site;
        }
    }
    return $sites;
}


function xml_escape($s)
{
    if ( empty($s) ) return '';
    $s = html_entity_decode($s, ENT_QUOTES, 'UTF-8');
    $s = htmlspecialchars($s, ENT_QUOTES, 'UTF-8', false);
    return $s;
}





/**
 * Returns a safe file from a user filename. ( User filename may have characters that are not  supported. like Korean characher ).
 *
 * @param $filename
 *
 * @return string
 */
function safe_filename($filename) {
    $pi = pathinfo($filename);
    $sanitized = md5($pi['filename'] . ' ' . $_SERVER['REMOTE_ADDR'] . ' ' . time());
    if ( isset($pi['extension']) && $pi['extension'] ) return $sanitized . '.' . $pi['extension'];
    else return $sanitized;
}

/**
 *
 * Returns the first Wordpress error from error object.
 *
 * @warning PHP `each` expression is deprecated as of 7.2.
 * @todo use other express than `each`
 *
 * @param $wp_error_object
 *
 * @return string
 */
function get_first_error_message( $wp_error_object ) {
    if ( ! is_wp_error($wp_error_object) ) return null;
    list ( $k, $v ) = each ($wp_error_object->errors);
    if ( $v ) return $v;
    else return null;
}


/**
 * Returns an uploaded file info.
 * @param $post_ID - the attachment post id.
 *
 * @todo update thumbnail url
 * @return array
 */
function get_uploaded_file($post_ID) {

    $post = get_post( $post_ID );
    $ret = [
        'guid' => $post->guid, // url
        'id' => $post->ID, // wp_posts.ID
//        'status' => $post->post_status,
//        'author' => $post->post_author,
//        'type' => $post->post_type,
        'media_type' => strpos( $post->post_mime_type, 'image/' ) === 0 ? 'image' : 'file',
        'mime_type' => $post->post_mime_type,
        'name' => $post->post_name,
//        'post' => $post->post_parent
    ];
    if ( $ret['media_type'] == 'image' ) {
        $ret['thumbnail_url'] = $post->guid; // thumbnail url
    }
    return $ret;
}

/**
 * Returns uploaded files of a post.
 *
 * @param $parent_ID
 * @param string $post_type
 * @return array
 * @example
 *      $files = get_uploaded_files(129);
 * print_r($files);
 */
function get_uploaded_files( $parent_ID, $post_type = 'attachment' ) {
    $ret = [];

    $files = get_children( ['post_parent' => $parent_ID, 'post_type' => $post_type] );
    if ( $files ) {
        foreach( $files as $file ) {
            $ret[] = get_uploaded_file($file->ID);
        }
    }

    return $ret;
}


function api_get_comment( $comment_ID ) {
    global $wpdb;

//    $comment = get_comment($comment_ID);

    $comment = $wpdb->get_row("SELECT * FROM {$wpdb->comments} WHERE comment_ID=$comment_ID");
    $ret = [
        'comment_ID' => $comment->comment_ID,
        'comment_post_ID' => $comment->comment_post_ID,
        'comment_parent' => $comment->comment_parent,
        'comment_author' => $comment->comment_author,
        'comment_date' => $comment->comment_date,
        'comment_content' => $comment->comment_content,
        'user_id' => $comment->user_id,
        'files' => get_uploaded_files($comment_ID, COMMENT_ATTACHMENT)
    ];
    debug_log('comment: ', $ret);
    return $ret;
}


global $nest_comments;
function get_nested_comments( $post_ID )
{
    global $nest_comments;
    $nest_comments = [];
    $post = get_post( $post_ID );

    if ( ! $post->comment_count ) return [];
    $comments = get_comments(['post_id' => $post_ID]);
//    return $comments;

    $comment_html_template = wp_list_comments(
        [
            'max_depth' => 100,
            'reverse_top_level' => 'asc',
            'avatar_size' => 0,
            'callback' => 'get_nested_comments_with_meta',
            'echo' => false
        ],
        $comments);
    return $nest_comments;
}




function get_nested_comments_with_meta( $comment, $args, $depth ) {
    global $nest_comments;
    $parent_comment = null;
    $comment->depth = $depth;
    unset(
        $comment->comment_agent,
        $comment->comment_author_IP,
        $comment->comment_author_email,
        $comment->comment_date_gmt,
        $comment->comment_karma
    );
    $comment->comment_ID = intval( $comment->comment_ID );
    $comment->comment_parent = intval( $comment->comment_parent );
    $comment->comment_post_ID = intval( $comment->comment_post_ID );
    $comment->user_id = intval( $comment->user_id );
    $comment->files = get_uploaded_files( $comment->comment_ID, COMMENT_ATTACHMENT );
    $nest_comments[] = $comment;
}



function api_hook_comment_attachments($comment_ID, $files) {
    if ( ! $files ) return;
    foreach( $files as $id ) {
        wp_update_post([ 'ID' => $id, 'post_parent' => $comment_ID, 'post_type' => COMMENT_ATTACHMENT ]);
    }
}


function api_delete_file($post_id) {
    global $wpdb;
    $result = $wpdb->delete( $wpdb->posts, array( 'ID' => $post_id ) );
    if ( ! $result ) {
        return false;
    }
    $file = get_attached_file( $post_id );
    $meta = wp_get_attachment_metadata( $post_id );
    $backup_sizes = get_post_meta( $post_id, '_wp_attachment_backup_sizes', true );
    $re = wp_delete_attachment_files( $post_id, $meta, $backup_sizes, $file );
    return $re;
}
