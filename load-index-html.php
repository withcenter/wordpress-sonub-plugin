<?php


if ( isRootSite() ) {
    include ABSPATH . 'root-site/index.html';
} else {
    include ABSPATH . 'user-site/index.html';
}
exit;
