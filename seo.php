<?php
/**
 * @file seo.php
 * @description @see https://docs.google.com/document/d/1nOEJVDilLbF0sNCkkRGcDwdT3rDLZp3h59oQ77BIdp4/edit#heading=h.30epwqdpfu8r
 */
include_once "library.php";
/**
 * Is the request for index.html for desktop ?
 */
if ( ! isRequestingIndex() ) {
	return;
}
if ( $_SERVER['REQUEST_URI'] == '/hello-world/' ) {
    return;
}
include 'load-index-html.php';
