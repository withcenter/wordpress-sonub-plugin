<?php
/**
 * @file sitemap.php
 */
/**
 * @see
 */
include_once '../../../wp-load.php';
include_once "library.php";
if ( isRootSite() ) {
    include './sitemaps/root-sitemap.php';
} else {
    include './sitemaps/user-sitemap.php';
}
