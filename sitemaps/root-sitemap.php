<?php
header('Content-type: application/xml; charset=utf-8');
/**
 *
sitemap in xml.

for root, all forum, all menu includeing menu page, login page, register page, help page and all page<br>

for blog, categories and maximum of last 1,000 posts.

 */


$date = date('Y-m-d');
echo <<<EOH
<?xml version="1.0" encoding="UTF-8"?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <sitemap>
      <loc>https://www.sonub.com/sitemap.xml</loc>
      <lastmod>$date</lastmod>
   </sitemap>
EOH;


$sites = getActiveSites();
foreach( $sites as $site ) {
    $date = date('Y-m-d', $site['stamp_update']);
    foreach( $site['domains'] as $domain ) {
        echo <<<EOH
        
   <sitemap>
      <loc>https://$domain/sitemap.xml</loc>
      <lastmod>$date</lastmod>
   </sitemap>
EOH;

    }
}
echo <<<EOE

</sitemapindex>
EOE;
