<?php
header('Content-type: application/xml; charset=utf-8');
/**
 *
 * This sitemap xml is only for 1 site.
 */
$domain = $_SERVER['HTTP_HOST'];
$site = domainSite($domain);

if ( empty($site) ) return;




if ( $site && isset($site['stamp_update']) ) { 
	$stamp = $site['stamp_update'];
} else {
	$stamp = $site['stamp_create'];
}
$date = date('Y-m-d', $stamp);


/**
 * Sitemap Header for each blog
 */
echo <<<EOH
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<url>
		<loc>https://$domain/</loc>
		<lastmod>$date</lastmod>
		<changefreq>always</changefreq>
		<priority>0.8</priority>
	</url>
EOH;


$args = array(
    'post_type' => 'post',
    'cat' => $site['root_category_term_id'],
    'posts_per_page' => 1000,
);
$q = new WP_Query( $args );
if ( $q->have_posts() ) {
    while ( $q->have_posts() ) {
        $q->the_post();
        $ID = get_the_ID();
        $date = get_the_date('Y-m-d');
        $title = xml_escape( get_the_title() );
        echo <<<EOH
        
	<url>
		<loc>https://$domain/post/$ID/$title</loc>
		<lastmod>$date</lastmod>
		<changefreq>weekly</changefreq>
		<priority>0.8</priority>
	</url>
EOH;
    }
}




echo "</urlset>";
