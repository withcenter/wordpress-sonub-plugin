<?php
/**
 * @file start.php
 * @description @see https://docs.google.com/document/d/1nOEJVDilLbF0sNCkkRGcDwdT3rDLZp3h59oQ77BIdp4/edit#heading=h.30epwqdpfu8r
 */

/**
 * Is the request for index.html for mobile ?
 * If file exists, Nginx will load that file.
 * So, only rewrite url will be checked.
 */
include_once 'library.php';
if ( ! isRequestingIndex() ) return;
require_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;

/**
 * index.html is requested for mobile?
 */
if (  $detect->isMobile() ) include 'load-index-html.php';
