<?php
include 'test-library.php';
if ( ! function_exists('wp_insert_category') ) require_once (ABSPATH . "/wp-admin/includes/taxonomy.php");

$k = "category-meta-update";
$arr = [
    'cat_name' => $k,
    'category_description' => $k,
    'category_nicename' => $k,
    'category_parent' => '',
    'taxonomy' => 'category'
];
$cat_ID = wp_insert_category( $arr, true );
if ( ! $cat_ID || is_wp_error($cat_ID)) {
    $cat = get_category_by_slug($k);
    $cat_ID = $cat->term_id;
}

echo "cat_ID: $cat_ID\n";

/**
 * apple 키의 값을 red 로 지정
 */
update_term_meta( $cat_ID, 'apple', 'red');
/**
 * 그리고 apple 키의 값을 blue 로 덮어 씀. 따라서 중복 입력이 안됨.
 */
update_term_meta( $cat_ID, 'apple', 'blue');
/**
 * 중복 입력을 하려면 add_term_meta() 를 사용해야 함.
 */
add_term_meta( $cat_ID, 'apple', 'from Korea');

// $cat_ID 의 모든 meta data 가져오기
$re = get_term_meta( $cat_ID );

/**
 * 주의: 실행을 할수록 결과는 아래와 같이 된다.
    [apple] => Array
        (
            [0] => blue
            [1] => blue
            [2] => blue
            [3] => blue
            [4] => blue
            [5] => blue
            [6] => blue
            [7] => from Korea
        )
 * 이렇게 되는 이유는
 *  'from Korea' 가 계속 추가되는데,
 *  다시 실행을 하면 'blue' 로 모두 업데이트를 해 버리기 때문이다.
 *  update_xxxx_meta() 함수에서 네번째 항목에서 업데이터를 원하는 값을 써 주지 않으면,
 *  해당 키의 모든 값들을 동일한 값으로 업데이트를 해 버리기 때문이다.
 */
print_r($re);
update_term_meta( $cat_ID, 'banana', 'yellow');

// $cat_ID 의 banana meta data 만 가져오기
$re = get_term_meta( $cat_ID, 'banana' );

print_r($re);


