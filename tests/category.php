<?php
include 'test-library.php';

$category = get_category_by_slug( 'site-12' );
$args = array(
    'parent'                 => $category->term_id,
    'hide_empty'               => false,
);
$child_categories = get_categories($args );

$res = [];
foreach( $child_categories as $c ) {
    $res[] = ['cat_ID' => $c->cat_ID, 'name' => $c->name];
}

print_r($res);
