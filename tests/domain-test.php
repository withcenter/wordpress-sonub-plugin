<?php
include 'test-library.php';

$domain = 'my.domain.com';
domainDelete($domain);
domainCreate('my.domain.com', 1, 1) ? testSuccess('my.domain.com created') : testError('failed to create my.domain.com');

domainCountProgress(1) == 1 ? testSuccess('There is 1 progress domain') : testError('No progressing domain?');
