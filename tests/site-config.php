<?php
include 'test-library.php';


$re = siteConfigExists(123456789, 'wrong-code');

$re === false ? testSuccess("siteConfigExists() must return false on wrong code") : testError("Got value $re when expected null");

$re = siteConfig(123456789, 'wrong-code');
$re === null ? testSuccess("siteConfig() must return null on wrong code") : testError("Got value $re when expected null");

siteConfigSave(1, 'code1', 'value1');
siteConfigExists(1, 'code1') === true ? testSuccess("siteConfigSave() ok") : testError("siteConfigSave() Got wrong value " . siteConfigExists(1, 'code1') );
siteConfig(1, 'code1') === 'value1' ? testSuccess("siteConfig() ok") : testError("siteConfig() Got wrong value " . siteConfigExists(1, 'code1') );

siteConfigSave(1, 'code1', 'value2');
siteConfig(1, 'code1') === 'value2' ? testSuccess("siteConfigSave() for update ok") : testError("siteConfigSave() error on update" );







