-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2018 at 09:51 AM
-- Server version: 10.3.9-MariaDB
-- PHP Version: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sonub_v2019`
--

-- --------------------------------------------------------

--
-- Table structure for table `sonub_domain_application`
--

CREATE TABLE `sonub_domain_application` (
  `idx` int(10) UNSIGNED NOT NULL,
  `idx_site` int(10) UNSIGNED NOT NULL,
  `user_ID` int(10) UNSIGNED NOT NULL,
  `domain` varchar(128) NOT NULL,
  `stamp_apply` int(10) UNSIGNED NOT NULL,
  `stamp_commit` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` char(1) NOT NULL DEFAULT '',
  `reason` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sonub_domain_application_log`
--

CREATE TABLE `sonub_domain_application_log` (
  `domain` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `stamp` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sonub_site_config`
--

CREATE TABLE `sonub_site_config` (
  `idx` int(10) UNSIGNED NOT NULL,
  `user_ID` int(10) UNSIGNED NOT NULL,
  `stamp_create` int(10) UNSIGNED NOT NULL,
  `stamp_update` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `sonub_site_config_meta`
--

CREATE TABLE `sonub_site_config_meta` (
  `idx` int(10) UNSIGNED NOT NULL,
  `idx_site` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `sp_files`
--

CREATE TABLE `sp_files` (
  `idx` int(10) UNSIGNED NOT NULL,
  `user_no` int(10) UNSIGNED NOT NULL,
  `relation` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(32) NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `complete` char(1) NOT NULL,
  `path` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `stamp_created` int(10) UNSIGNED NOT NULL,
  `stamp_updated` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_tests`
--

CREATE TABLE `sp_tests` (
  `idx` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `address` varchar(128) NOT NULL DEFAULT '',
  `stamp_created` int(11) NOT NULL,
  `stamp_updated` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sonub_domain_application`
--
ALTER TABLE `sonub_domain_application`
  ADD PRIMARY KEY (`idx`),
  ADD UNIQUE KEY `domain` (`domain`),
  ADD KEY `status` (`status`),
  ADD KEY `idx_site` (`idx_site`),
  ADD KEY `user_ID` (`user_ID`);

--
-- Indexes for table `sonub_site_config`
--
ALTER TABLE `sonub_site_config`
  ADD PRIMARY KEY (`idx`),
  ADD KEY `user_ID` (`user_ID`),
  ADD KEY `stamp_create` (`stamp_create`),
  ADD KEY `stamp_update` (`stamp_update`);

--
-- Indexes for table `sonub_site_config_meta`
--
ALTER TABLE `sonub_site_config_meta`
  ADD PRIMARY KEY (`idx`),
  ADD UNIQUE KEY `idx_site_code` (`idx_site`,`code`),
  ADD KEY `code` (`code`),
  ADD KEY `idx_site` (`idx_site`);

--
-- Indexes for table `sp_files`
--
ALTER TABLE `sp_files`
  ADD PRIMARY KEY (`idx`),
  ADD KEY `user_no` (`user_no`),
  ADD KEY `relation` (`relation`),
  ADD KEY `code` (`code`);

--
-- Indexes for table `sp_tests`
--
ALTER TABLE `sp_tests`
  ADD PRIMARY KEY (`idx`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sonub_domain_application`
--
ALTER TABLE `sonub_domain_application`
  MODIFY `idx` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sonub_site_config`
--
ALTER TABLE `sonub_site_config`
  MODIFY `idx` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sonub_site_config_meta`
--
ALTER TABLE `sonub_site_config_meta`
  MODIFY `idx` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_files`
--
ALTER TABLE `sp_files`
  MODIFY `idx` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_tests`
--
ALTER TABLE `sp_tests`
  MODIFY `idx` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
