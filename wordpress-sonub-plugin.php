<?php
/**
 * Plugin Name: Wordpress Sonub Plugin
 * Description: Sonub supporting plugin for Restful Api and Client end supporting.
 * Author: JaeHo Song
 * Author URI: https://jaeho.sonub.com
 * Version: 0.1
 * Plugin URI: https://jaeho.sonub.com
 */


include_once 'library.php';




add_action( 'wp', function( $wp ) {
    $path = 'seo.php';
    debug_log('wp hooks: run seo.php at ' . $path);
    require( $path );
} );



function error( $code, $message ) {
    return new WP_Error( $code, $message, array( 'status' => 400 ) );
}
/**
 * This is being called whenever Rest Api is access to authenticate the user.
 *
 * @param $user
 * @return bool|int|mixed|null|WP_Error|WP_User
 */
function json_basic_auth_handler( $user ) {
    global $wp_json_basic_auth_error;

    $wp_json_basic_auth_error = null;

//    debug_log("json_basic_auth_handler(user)", $user);
    // Don't authenticate twice
    if ( ! empty( $user ) ) {
        return $user;
    }

    // Check that we're trying to authenticate
    if ( !isset( $_SERVER['PHP_AUTH_USER'] ) ) {
        return $user;
    }

    $username = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];


    debug_log("username: $username, password: $password");
    if ( ! $username || $username == 'null' || $username == 'undefined' ) {
        debug_log("username is not set. just return null in json_basic_auth_handler");
        return null;
    }

    debug_log("Username is set. Going to check username");

    /**
     * Hacked by JaeHo Song.
     * Check if the user is using security code instead of plain password.
     */
    if ( is_numeric($username) ) {
        $user = get_userdata($username);
	if ( ! $user ) {
		$wp_json_basic_auth_error = error('login_session_invalid', 'Login sessio invalid');
		return null;
	}
        $security_code = get_security_code( $user->ID );
        if ( $password == $security_code ) {
            wp_set_current_user($user->ID);
            $wp_json_basic_auth_error = true;
            return $user->ID;
        }
    }

    /**
     * In multi-site, wp_authenticate_spam_check filter is run on authentication. This filter calls
     * get_currentuserinfo which in turn calls the determine_current_user filter. This leads to infinite
     * recursion and a stack overflow unless the current function is removed from the determine_current_user
     * filter during authentication.
     */
    remove_filter( 'determine_current_user', 'json_basic_auth_handler', 20 );

    $user = wp_authenticate( $username, $password );

    add_filter( 'determine_current_user', 'json_basic_auth_handler', 20 );

    if ( is_wp_error( $user ) ) {
        $wp_json_basic_auth_error = $user;
        debug_log("wp_authenticate error: username: $username, password: $password", $user);
        return null;
    }

    $wp_json_basic_auth_error = true;

    return $user->ID;
}
add_filter( 'determine_current_user', 'json_basic_auth_handler', 20 );

/**
 * @param $error
 * @return mixed
 */
function json_basic_auth_error( $error ) {
    if ( $error ) debug_log("json_basic_auth_error", $error);
    // Passthrough other errors
    if ( ! empty( $error ) ) {
        return $error;
    }

    global $wp_json_basic_auth_error;

    return $wp_json_basic_auth_error;
}
add_filter( 'rest_authentication_errors', 'json_basic_auth_error' );


/**
 * This allow anyone can register into wordpress through Rest Api.
 *
 * author_cap_filter()
 *
 * Filter on the current_user_can() function.
 * This function is used to explicitly allow authors to edit contributors and other
 * authors posts if they are published or pending.
 *
 * @param array $allcaps All the capabilities of the user
 * @param array $cap [0] Required capability
 * @param array $args [0] Requested capability
 *                       [1] User ID
 *                       [2] Associated object ID
 * @return array
 */
function give_permissions( $allcaps, $cap, $args ) {
    //    $allcaps['rest_cannot_create'] = true; // This is not working. 이것을 해도 사용자가 Rest Api 로 글 작성 할 수 없음. 그래서 아래와 같이 회원 가입하면 권한을 줌.
    //    $allcaps[$cap[0]] = true; // This is wrong. 이렇게 하면, 모든 요청되는 권한을 주므로 안된다.

    /**
     * Allow anyone can register
     */
    $allcaps['create_users'] = true;

    /**
     * Allow user update only his user data. Not others.
     * $args[1] is login user's ID.
     * $args[2] is the user ID that will be updated.
     */
    if ( isset($args[2]) && $args[1] == $args[2] ) {
        $allcaps['edit_users'] = true;
    }


//    $allcaps['edit_post'] = false;            // This is not working for REST API.
    $allcaps['edit_others_posts'] = false;      // This only lets the owner of the post can edit the post.

//    debug_log('Required capability: ', $cap[0]);
//    debug_log('args: ', $args);


    return $allcaps;
}
add_filter( 'user_has_cap', 'give_permissions', 10, 3 );

add_filter('duplicate_comment_id', '__return_false');

/**
 * This function is being invoked right after user registered.
 * It gives 'editor' role to newly registered users so they can create posts.
 *
 * @param $user_id
 */
function do_user_register( $user_id ) {
    $user = new WP_User($user_id);
    $user->remove_role('subscriber');
    $user->add_role('editor');
}
add_action( 'user_register', 'do_user_register', 10, 1 );


/**
 * It returns user's security code every 'user' Rest Api call.
 * @note When user is not logged in(Especially when user has registered), it compares if the user is the last user who registered to wordpress,
 *          If the user is the last user ( Meaning, just registered, returns security code also.
 * It only returns if my user information is requested. Meaning security code for others will not be returned.
 */
register_rest_field( 'user', 'security_code',
    array(
        'get_callback'    => function ( $user ) {
            if ( $user['id'] == wp_get_current_user()->ID || $user['id'] == get_last_user_ID() ) {
                return get_security_code( $user['id'] );
            }
        },
        'update_callback' => null,
        'schema'          => null,
    )
);

/**
 * Returns the user security code.
 *
 * @param int $ID User ID
 * @return string
 */
function get_security_code( $ID ) {
    $user = get_userdata($ID);
    $security_source = "{$user->ID},{$user->user_email},{$user->user_registered},{$user->user_pass}";
    return md5($security_source);
}


/**
 * Returns user ID of last registered user.
 * @return mixed
 */
function get_last_user_ID() {
    $args = array(
//        'role'         => 'author', // authors only
        'orderby'      => 'registered', // registered date
        'order'        => 'DESC', // last registered goes first
        'number'       => 1 // limit to the last one, not required
    );

    $users = get_users( $args );

    $last_user_registered = $users[0]; // the first user from the list

    return $last_user_registered->ID;
}

/**
 * Register a new POST TYPE for comment attachment.
 */
add_action( 'init', function() {
    $args = [ 'public' => true, 'label' => __('Comment Media', COMMENT_ATTACHMENT)];
    register_post_type(COMMENT_ATTACHMENT, $args);
});

/**
 * 글 생성 후 또는 업데이트 후 훅.
 */
add_action( 'rest_insert_post', function( WP_Post $post, WP_REST_Request $request, $creating ) {
    $file_IDs = $request->get_param('files_ID');
    if ( $file_IDs ) {
        foreach( $file_IDs as $id ) {
            wp_update_post([ 'ID' => $id, 'post_parent' => $post->ID ]);
        }
    }
}, 10, 3);

add_action( 'rest_prepare_post', function(WP_REST_Response $response, WP_Post $post, WP_REST_Request $request) {
    $old = $response->get_data();


    $data = [
        'id' => $old['id'],
        'date' => $old['date'],
        'guid' => $old['guid']['rendered'],
        'modified' => $old['modified'],
        'title' => $old['title']['rendered'],
        'content' => $old['content']['rendered'],
        'author' => $old['author'],
        'meta' => $old['meta'],
        'slug' => $old['slug'],
        'categories' => $old['categories'],
        'files' => get_uploaded_files($old['id']),
        'comments' => get_nested_comments( $old['id'] )
    ];
    $response->set_data($data);
    $response->remove_link( 'about' );
    $response->remove_link( 'author' );
    $response->remove_link( 'collection' );
    $response->remove_link( 'curies' );
    $response->remove_link( 'replies' );
    $response->remove_link( 'self' );
    $response->remove_link( 'version-history' );
    $response->remove_link( 'wp:attachment' ); // 없어지지 않음. 다른 뭔가 있는 것 같음.
    $response->remove_link( 'wp:term' ); // 없어지지 않음. 다른 뭔가 있는 것 같음.
    return $response;
}, 10, 3);


/**
 * Returns login user's data.
 */
add_action( 'rest_api_init', function () {


    /**
     * Returns login user's data.
     */
        register_rest_route(SONUB_ROUTE_PATH, '/version', array(
            'methods' => 'GET',
            'callback' => function () {
                return 'v2019.01';
            }
        ));


    register_rest_route(SONUB_ROUTE_PATH, '/profile', array(
        'methods' => 'GET',
        'callback' => function () {

            if (is_user_logged_in()) {
                $user = wp_get_current_user();
                $re = [
                    'email' => $user->user_email,
                    'id' => $user->ID,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'name' => $user->display_name,
                    'nickname' => $user->nickname,
                    'meta' => '',
                    'register_date' => $user->user_registered,
                    'username' => $user->user_login,
                    'security_code' => get_security_code($user->ID)
                ];

                return $re;
            } else {
                return null;
            }
        },
    ));


    register_rest_route( SONUB_ROUTE_PATH, '/system-settings', array(
        'methods' => 'GET',
        'callback' => function(WP_REST_Request $req) {
            $config = sonub_config();
            $domain = $req->get_param('domain');

            if ( $domain ) {
                $config['domain'] = $domain;
                $config['site'] = domainSite($domain);
            }
            return $config;
        },
    ));

    register_rest_route( SONUB_ROUTE_PATH, '/sites', array(
        'methods' => 'GET',
        'callback' => function() {

            if ( ! is_user_logged_in() ) {
                return error(ERROR_LOGIN_FIRST, 'login first');
            }
            $user = wp_get_current_user();

            $data = [
                MAX_SITES_CODE => MAX_SITES,
                MAX_DOMAINS_CODE => MAX_DOMAINS,
                AVAILABLE_SITES_CODE => availableSites( $user->ID ),
                AVAILABLE_DOMAINS_CODE => availableDomains( $user->ID ),
                NO_OF_DOMAINS_IN_PROGRESS => domainCountProgress($user->ID),
                'sites' => sites( $user->ID )
            ];

            return $data;
        },
    ));

    register_rest_route( SONUB_ROUTE_PATH, '/site', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req ) {
            return site( $req->get_param('idx_site') );
        },
    ));


    /**
     * create a site with a domain
     *
     * @note domain must be a valid one.
     *      - 'www.sonub.com', 'apple.com'
     */
    register_rest_route( SONUB_ROUTE_PATH, '/create-site', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {

            $domain = $req->get_param('domain');
            $name = $req->get_param('name');
            $description = $req->get_param('description');

            if ( ! $domain ) return error(ERROR_BAD_PARAM, 'domain is not provided');
            if ( ! isValidDomain( $domain ) ) return error(ERROR_MALFORMED_DOMAIN, "You have input wrong domain");

            if ( ! $name ) return error(ERROR_BAD_PARAM, 'name is not provided');
            if ( ! is_user_logged_in() ) return error(ERROR_LOGIN_FIRST, 'login first');

            $user = wp_get_current_user();

            if ( siteCount( $user->ID ) >= MAX_SITES ) return error(ERROR_MAX_SITES, "You cannot create more than " . MAX_SITES . " sites");
//            if ( domainCount( $user->ID ) >= MAX_DOMAINS ) return error(ERROR_MAX_SITES, "You cannot create more than " . MAX_SITES . " sites");
            if ( domainCount( $user->ID ) >= MAX_DOMAINS ) return error(ERROR_MAX_DOMAINS, "You cannot add more than " . MAX_DOMAINS . " domains");
            if ( domainExists( $domain ) ) return error(ERROR_DOMAIN_EXISTS, "Domain name is already in use");

            $idx_site = siteCreate([
                'user_ID' => $user->ID,
                'name' => $name,
                'description' => $description
            ]);

            if ( ! domainApply( $domain, $idx_site, $user->ID ) ) {
                return error(ERROR_DOMAIN_INSERT, 'Site created! But failed to connect to domain.');
            }

            return site( $idx_site );
        },
    ));
    /**
     * updates a site
     *
     * @desc This saves parts of site data. It saves any data coming from client.
     */
    register_rest_route( SONUB_ROUTE_PATH, '/update-site', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {

            if ( ! is_user_logged_in() ) return error(ERROR_LOGIN_FIRST, 'login first');
            $user = wp_get_current_user();


            $params = $req->get_params();
            debug_log( 'update-site: request: ', $params);
            if ( ! isset( $params['idx'] ) ) return error(ERROR_BAD_PARAM, 'site.idx is not provided');
            if ( ! siteMine($params['idx'], $user->ID) ) return error(ERROR_NOT_YOUR_SITE, 'You cannot edit. The site is owned by someone else.');

            $idx = $params['idx'];
            unset( $params['idx'] );

            db()->update(SITE_CONFIG, ['stamp_update' => time()], ['idx' => $idx]);
            siteConfigSaveArray( $idx, $params );
            return site( $idx );
        },
    ));

    /**
     * adds a domain to a site
     *
     * @see
     *
     */
    register_rest_route( SONUB_ROUTE_PATH, '/add-domain', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {
            $in = [
                'domain' => $req->get_param('domain'),
                'idx_site' => $req->get_param('idx_site')
            ];
            $re = domainAttachToSite( $in );
            if ( $re ) return $re;
            else return $in;
        },
    ));

    /**
     * deletes a domain
     *
     * @see
     *
     */
    register_rest_route( SONUB_ROUTE_PATH, '/delete-domain', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {

            $domain = $req->get_param('domain');

            if ( ! $domain ) return error(ERROR_BAD_PARAM, 'domain is not provided');
            if ( ! is_user_logged_in() ) return error(ERROR_LOGIN_FIRST, 'login first');

            $user = wp_get_current_user();

            if ( ! domainMine( $domain, $user->ID ) ) return error(ERROR_NOT_YOUR_DOMAIN, 'This is not your domain');

            $row = domainGet($domain);





            if ( $row['status'] == 'S' || $row['status'] == 'F' ) {

            } else if ( $row['stamp_apply'] == 0 ) {
                return error(ERROR_DOMAIN_SETUP_NOT_YET_BEGIN, 'Domain setup has not bet begun. please wait.');
            } else if ( $row['stamp_apply'] > time() - 60 * 60 * 3 ) {
                return error(ERROR_DOMAIN_SETUP_IN_PROGRESS, 'Domain setting up is in progress. You can delete after 3 hours if it is not success.');
            }


            
            domainDelete( $domain );


            return ['domain' => $domain];
        },
    ));



    /**
     * create a category with category count.
     *
     * @note when a category is created, it is saved at the last of the category list.
     *
     * @see
     *
     */
    register_rest_route( SONUB_ROUTE_PATH, '/create-category', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {
            if ( ! is_user_logged_in() ) return error(ERROR_LOGIN_FIRST, 'login first');
            $user = wp_get_current_user();

            $idx_site = $req->get_param('idx_site');
            $name = $req->get_param('name');

            if ( ! function_exists('wp_insert_category') ) require_once (ABSPATH . "/wp-admin/includes/taxonomy.php");

            $k = "site-$idx_site";
            $parent_category = get_category_by_slug($k);
            if ( !$parent_category ) {
                $arr = [
                    'cat_name' => $k,
                    'category_description' => $k,
                    'category_nicename' => $k,
                    'category_parent' => '',
                    'taxonomy' => 'category'
                ];
                $ID = wp_insert_category( $arr, true );
                if ( is_wp_error( $ID ) ) {
                    $msg = $ID->get_error_message();
                    return error(ERROR_SLUG_CREATE_PARENT, "Failed to create slug: $msg");
                }

                $parent_category = get_category_by_slug($k);
                if ( !$parent_category ) {
                    return error(ERROR_SLUG_CREATE_PARENT, 'Failed to get parent slug');
                }
            }
            $parent_category_id = $parent_category->term_id;



            $no = get_option(SLUG_COUNT);
            if ( ! $no ) $no = 0;
            $no ++;
            $re = update_option(SLUG_COUNT, $no, false);
            if ( ! $re ) {
                return error(ERROR_SLUG_COUNT_UPDATE, 'Failed to update slug count');
            }


            $arr = [
                'cat_name' => $name,
                'category_description' => "",
                'category_nicename' => $no,
                'category_parent' => $parent_category_id,
                'taxonomy' => 'category'
            ];

            $ID = wp_insert_category( $arr, true );
            if ( is_wp_error( $ID ) ) {
                $msg = $ID->get_error_message();
                return error(ERROR_SLUG_CREATE, "Failed to create slug: $msg");
            }

            $orders = siteConfig( $idx_site, TERMS_ID_IN_ORDER );
            if ( $orders ) $orders .= ",$ID";
            else $orders = $ID;
            siteConfigSave( $idx_site, TERMS_ID_IN_ORDER, $orders);

            add_term_meta( $ID, 'owner', $user->ID, true);


            return ['idx_site' => $idx_site, 'name' => $name, 'term_id' => $ID, 'orders' => $orders];

        },
    ));



    /**
     * delete a category
     *
     * @note the category must belong to the user.
     *
     * @see
     *
     * @todo check if category exists and if not, return proper error message.
     *
     */
    register_rest_route( SONUB_ROUTE_PATH, '/delete-category', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {
            $term_id = $req->get_param('term_id');
            if ( ! is_user_logged_in() ) return error(ERROR_LOGIN_FIRST, 'login first');
            $user = wp_get_current_user();

            // check if my category
            $user_ID = get_term_meta( $term_id, 'owner', true );
            debug_log("my_ID: {$user->ID}, owner_ID: $user_ID");
            if ( $user->ID != $user_ID ) {
                return error(CODE_NOT_YOUR_CATEGORY, "This is not your category. You cannot delete");
            }

            // delete
            wp_delete_category( $term_id );

            return [ 'term_id' => $term_id ];

        },
    ));



    /**
     * rename a category
     *
     * @note the category must belong to the user.
     *
     * @see
     *
     * @todo check if category exists and if not, return proper error message.
     *
     */
    register_rest_route( SONUB_ROUTE_PATH, '/rename-category', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {
            if ( ! is_user_logged_in() ) return error(ERROR_LOGIN_FIRST, 'login first');
            $user = wp_get_current_user();

            $term_id = $req->get_param('term_id');
            $name = $req->get_param('name');

            if ( ! function_exists('wp_update_category') ) require_once (ABSPATH . "/wp-admin/includes/taxonomy.php");


            $user_ID = get_term_meta( $term_id, 'owner', true );
            debug_log("my_ID: {$user->ID}, owner_ID: $user_ID");
            if ( $user->ID != $user_ID ) {
                return error(CODE_NOT_YOUR_CATEGORY, "This is not your category. You cannot update");
            }


            // update category
            wp_update_category([
                'cat_ID' => $term_id,
                'cat_name' => $name
            ]);
            return ['name' => $name, 'term_id' => $term_id];

        },
    ));



    /**
     * sort categories
     *
     * @see
     *
     */
    register_rest_route( SONUB_ROUTE_PATH, '/sort-categories', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {
            $idx_site = $req->get_param('idx_site');
            $orders = $req->get_param('orders');
//            update_option(TERMS_ID_IN_ORDER . $idx_site, $orders, false);

            siteConfigSave($idx_site, TERMS_ID_IN_ORDER, $orders);
            return ['idx_site' => $idx_site ];

        },
    ));




    /**
     * file upload
     *
     * @reason After long time searching & digging,
     *          File upload is not working or don't know how to make it work with WP REST API.
     *          So, I made custom Api which is very simple and easy to build.
     *
     * @see https://docs.google.com/document/d/1nOEJVDilLbF0sNCkkRGcDwdT3rDLZp3h59oQ77BIdp4/edit#heading=h.8fvprdfjs0v5
     *
     */
    register_rest_route( SONUB_ROUTE_PATH, '/file-upload', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {

            $parent = $req->get_param('post_parent');
            debug_log("parent: $parent");
            $params = $req->get_file_params();
            $file = $params['file'];
            if ( $file['error'] ) return error( 'file_upload_error', $file['error']);

            // Prepare to save
            $file_type = wp_check_filetype( basename( $file["name"] ), null ); // get file type
            $file_name = safe_filename( $file["name"] ); // get save filename to save
            $dir = wp_upload_dir(); // Get WordPress upload folder.
            $file_path = $dir['path'] . "/$file_name"; // Get Path of uploaded file.
            $file_url = $dir['url'] . "/$file_name"; // Get Path of uploaded file.

            if ( ! move_uploaded_file( $file['tmp_name'], $file_path ) ) return error('failed_on_move_uploaded_file', "Failed on moving uploaded file." );


            // Create a post of attachment type.
            $attachment = array(
                'guid'              => $file_url,
                'post_author'       => wp_get_current_user()->ID,
                'post_mime_type'    => $file_type['type'],
                'post_name'         => $file['name'],
                'post_title'        => $file_name,
                'post_content'      => '',
                'post_status'       => 'inherit',
            );

            // This does not upload a file but creates a 'attachment' post type in wp_posts.
            $attach_id = @wp_insert_attachment( $attachment, $file_name, $parent );
            debug_log("attach_id: ", $attach_id);
            if ( $attach_id == 0 || is_wp_error( $attach_id ) ) return error( 'failed_to_attach_uploaded_file', get_first_error_message( $attach_id ) );
            update_attached_file( $attach_id, $file_path ); // update post_meta for the use of get_attached_file(), get_attachment_url();
            require_once ABSPATH . '/wp-admin/includes/media.php';
            require_once ABSPATH . '/wp-admin/includes/image.php';

            $attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );
            wp_update_attachment_metadata( $attach_id,  $attach_data );

            $ret = get_uploaded_file( $attach_id );
            return $ret;
        }

    ));

    register_rest_route( SONUB_ROUTE_PATH, '/file-delete', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {

            if ( ! is_user_logged_in() ) return error(ERROR_LOGIN_FIRST, 'login first');
            $post_id = $req->get_param('post_id');
            $post = get_post( $post_id );
            if ( $post->post_author != wp_get_current_user()->ID ) return error('not_your_file', 'This is not your file');



//
//            global $wpdb;
//
//            $result = $wpdb->delete( $wpdb->posts, array( 'ID' => $post_id ) );
//            if ( ! $result ) {
//                return false;
//            }
//
//            $file = get_attached_file( $post_id );
//            $meta = wp_get_attachment_metadata( $post_id );
//            $backup_sizes = get_post_meta( $post->ID, '_wp_attachment_backup_sizes', true );
//            $re = wp_delete_attachment_files( $post_id, $meta, $backup_sizes, $file );

            $re = api_delete_file($post_id);

            if ( $re === false ) return error('delete_failed', 'file delete failed');
            return ['id' => $post_id];
        }
    ));

    register_rest_route( SONUB_ROUTE_PATH, '/post-delete', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {

            if ( ! is_user_logged_in() ) return error(ERROR_LOGIN_FIRST, 'login first');
            $post_id = $req->get_param('post_id');
            $post = get_post( $post_id );
            if ( $post->post_author != wp_get_current_user()->ID ) return error('not_your_post', 'This is not your post.');

            $files = get_children( ['post_parent' => $post_id, 'post_type' => 'attachment'] );
            if ( $files ) {
                foreach( $files as $file ) {
                    wp_delete_attachment($file->ID, true);
                }
            }
            wp_delete_post($post_id, true);

            return ['id' => $post_id];
        }
    ));

    register_rest_route( SONUB_ROUTE_PATH, '/create-comment', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {
            $comment_parent = $req->get_param('comment_parent') ? $req->get_param('comment_parent') : 0;
            $comment_post_ID = $req->get_param('comment_post_ID') ? $req->get_param('comment_post_ID') : 0;
            $content = $req->get_param('comment_content');
            $current_user = wp_get_current_user();

            $commentdata = array(
                'comment_post_ID' => $comment_post_ID, // to which post the comment will show up
                'comment_author' => $current_user->nickname, //fixed value - can be dynamic
                'comment_author_email' => '', //fixed value - can be dynamic
                'comment_author_url' => '', //fixed value - can be dynamic
                'comment_content' => $content, //fixed value - can be dynamic
                'comment_type' => '', //empty for regular comments, 'pingback' for pingbacks, 'trackback' for trackbacks
                'comment_parent' => $comment_parent, //0 if it's not a reply to another comment; if it's a reply, mention the parent comment ID here
                'user_id' => $current_user->ID, //passing current user ID or any predefined as per the demand
            );

            //Insert new comment and get the comment ID
            $comment_id = wp_new_comment( $commentdata, true );
            if ( $comment_id === false ) {
                return error('failed_create_comment', 'failed to create comment');
            }
            if ( is_wp_error($comment_id) ) {
                $msg = $comment_id->get_error_message();
                debug_log('error on comment: ', $msg);
                if ( strpos( $msg, 'Duplicate comment detected' ) !== false ) {
                    // Okay. duplicate comment is okay.
                } else {
                    return error('cannot_create_comment', $msg );
                }
            }
            api_hook_comment_attachments($comment_id, $req->get_param('files_ID'));
            return api_get_comment( $comment_id );
        }
    ));
    register_rest_route( SONUB_ROUTE_PATH, '/update-comment', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {
            $comment_ID = $req->get_param('comment_ID');
            $comment = api_get_comment($comment_ID);
            if ( wp_get_current_user()->ID != $comment['user_id'] ) return error('not_your_comment', 'This is not your comment');
            $comment_content = $req->get_param('comment_content');
            global $wpdb;
            $up = [
                'comment_content' => $comment_content
            ];
            $cond = ['comment_ID' => $comment_ID];
            $re = $wpdb->update($wpdb->comments, $up, $cond);
            if ( $re === false ) return error('failed_comment_update', 'Failed to update comment');
            api_hook_comment_attachments($comment_ID, $req->get_param('files_ID'));
            return api_get_comment($comment_ID);


        }
    ));

    /**
     *
     * @warning This deletes a comment. If comments are threaded like below,
     *      -> comment 1
     *          -> comment 2
     *              -> comment 3
     *      and if 'comment 2' is deleted, then it looks like below.
     *      -> comment 1
     *              -> comment 3
     *      It does not look good.
     *      So, instead of delete, try to update with empty content or some 'delete mark'.
     */
    register_rest_route( SONUB_ROUTE_PATH, '/delete-comment', array(
        'methods' => 'POST',
        'callback' => function(WP_REST_Request $req) {
            $comment_ID = $req->get_param('comment_ID');
            $comment = api_get_comment($comment_ID);
            if ( wp_get_current_user()->ID != $comment['user_id'] ) return error('not_your_comment', 'This is not your comment');


            $files = get_children( ['post_parent' => $comment_ID, 'post_type' => COMMENT_ATTACHMENT] );
            if ( $files ) {
                foreach( $files as $file ) {
                    $re = api_delete_file($file->ID);
//                    wp_delete_attachment($file->ID, true);
                }
            }

            $re = wp_delete_comment( $comment_ID, true );
            if ( $re ) return ['comment_ID' => $comment_ID];
            else return error('failed_comment_delete', 'Failed to delete comment');
        }
    ));




} );
